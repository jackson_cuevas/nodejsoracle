const webServer = require('./services/web-server.js');

async function startup() {
    console.log('Staring application!');

    try {
        console.log('Initializin web server module');

        await webServer.initialize();
    } catch (error) {
        console.error(err);

        process.exit(1); // Non-zero failure code
    }
}

async function shutdown(e) {
    let err = e;

    console.log('Shutting down');

    try {
        console.log('Closing web server module');
        
        await webServer.close();
    } catch (e) {
        console.log('Encountered error', e);

        err = err || e;
    }
    console.log('exiting process');

    if (err) {
        process.exit(1); //Non-zero failure code
    } else {
        process.exit(0);
    }
}

process.on('SIGTERM', () => {
    console.log('Received SIGTERM');

    shutdown();
});

process.on('SIGINT', () => {
    console.log('Received SIGINI');

    shutdown();
});

process.on('uncaughtException', err => {
    console.log('Uncaught exception');
    console.log(err);

    shutdown(err);
});

startup();